import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../actions/userActions";
import { Grid, Row, Col, Alert } from "react-bootstrap";
import { withRouter } from "react-router-dom";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loggingError: false,
      loggingErrorMessage: "",
    };
  }

  componentWillReceiveProps(nextProps) {
    if ("token" in nextProps.user) {
      this.props.history.replace("admin");
    } else {
      this.setState({
        loggingError: true,
        loggingErrorMessage: nextProps.user.message,
      });
    }
  }

  handleSubmit = event => {
    event.preventDefault();

    const data = new FormData(event.target);
    const postBody = {
      email: data.get("login"),
      password: data.get("password"),
    };

    this.props.userActions.loginUser(postBody);
  };

  closeWarning = () => {
    this.setState({
      loggingError: false,
    });
  };

  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Login"
                content={
                  <form onSubmit={this.handleSubmit}>
                    {this.state.loggingError && (
                      <Alert bsStyle="warning">
                        <button
                          type="button"
                          aria-hidden="true"
                          className="close"
                          onClick={this.closeWarning}
                        >
                          &#x2715;
                        </button>
                        <span>
                          <b>Logowanie nie powiodło się </b> Zły login lub hasło
                        </span>
                      </Alert>
                    )}
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Login",
                          name: "login",
                          type: "text",
                          bsClass: "form-control",
                          placeholder: "Login",
                          defaultValue: "",
                          required: true,
                        },
                      ]}
                    />
                    <FormInputs
                      ncols={["col-md-12"]}
                      proprieties={[
                        {
                          label: "Hasło",
                          name: "password",
                          type: "password",
                          bsClass: "form-control",
                          placeholder: "Hasło",
                          defaultValue: "",
                          required: true,
                        },
                      ]}
                    />
                    <Button bsStyle="info" pullRight fill type="submit">
                      Zaloguj się
                    </Button>
                    <div className="clearfix" />
                  </form>
                }
              />
            </Col>
          </Row>
        </Grid>
        >
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Login)
);
