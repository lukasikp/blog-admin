import React, { Component } from "react";

import { HashRouter, Route, Switch } from "react-router-dom";

import indexRoutes from "../../routes/index.jsx";

class Logged extends Component {

  render() {
    return (
        <HashRouter>
             <Switch>
               {indexRoutes.map((prop, key) => {
                 return <Route to={prop.path} component={prop.component} key={key} />;
               })}
          </Switch>
        </HashRouter>
    );
  }
}

export default Logged;
