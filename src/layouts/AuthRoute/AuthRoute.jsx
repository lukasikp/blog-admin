import React, { Component } from "react";
import { connect } from "react-redux";

import Login from "../Login/Login";
import Admin from "../Admin/Admin";

class AuthRoute extends Component {
  render() {
    return <div>{this.props.user.token ? <Admin /> : <Login />}</div>;
  }
}
function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

export default connect(mapStateToProps)(AuthRoute);
