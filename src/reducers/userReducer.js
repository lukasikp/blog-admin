import initialState from "./initialState";
import {
  LOGIN_USER,
  RECEIVE_USER_TOKEN,
  FETCH_USER_DATA,
  RECEIVE_USER_DATA,
  UPDATE_USER_DATA,
  LOGOUT_USER,
} from "../actions/actionTypes";

export default function user(state = initialState.user, action) {
  let newState;
  switch (action.type) {
    case LOGIN_USER:
      return action;
    case RECEIVE_USER_TOKEN:
      newState = action.user;
      return newState;
    case FETCH_USER_DATA:
      return action;
    case RECEIVE_USER_DATA:
      newState = { ...state, data: action.data };
      return newState;
    case UPDATE_USER_DATA:
      return action;
    case LOGOUT_USER:
      newState = action.data;
      return newState;
    default:
      return state;
  }
}
