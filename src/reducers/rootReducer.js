import { combineReducers } from "redux";
import articles from "./articleReducer";
import categories from "./categoryReducer";
import user from "./userReducer";
import uploads from "./uploadsReducer";
import pages from "./pageReducer";

const rootReducer = combineReducers({
  articles,
  categories,
  user,
  uploads,
  pages,
});

export default rootReducer;
