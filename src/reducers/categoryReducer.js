import initialState from "./initialState";
import {
  FETCH_CATEGORY_LIST,
  RECEIVE_CATEGORY_LIST,
  REMOVE_CATEGORY,
  CREATE_CATEGORY,
  CATEGORY_SERVER_RESPONSE,
} from "../actions/actionTypes";

export default function categories(state = initialState.categories, action) {
  let newState;
  switch (action.type) {
    case FETCH_CATEGORY_LIST:
      return action;
    case RECEIVE_CATEGORY_LIST:
      return {
        ...state,
        categoryList: action.categories,
      };
    case CREATE_CATEGORY:
      return action;
    case REMOVE_CATEGORY:
      return action;
    case CATEGORY_SERVER_RESPONSE:
      return {
        ...state,
        categoryResponse: action.payload,
      };
    default:
      return state;
  }
}
