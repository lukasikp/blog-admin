import initialState from "./initialState";
import {
  FETCH_NAVBAR_LINKS_LIST,
  RECEIVE_NAVBAR_LINKS_LIST,
  REMOVE_NAVBAR_LINKS,
  CREATE_NAVBAR_LINKS,
  NAVBAR_LINKS_SERVER_RESPONSE,
} from "../actions/actionTypes";

export default function categories(state = initialState.navbarLinks, action) {
  let newState;
  switch (action.type) {
    case FETCH_NAVBAR_LINKS_LIST:
      return action;
    case RECEIVE_NAVBAR_LINKS_LIST:
      return {
        ...state,
        navbarLinksList: action.payload,
      };
    case CREATE_NAVBAR_LINKS:
      return action;
    case REMOVE_NAVBAR_LINKS:
      return action;
    case NAVBAR_LINKS_SERVER_RESPONSE:
      return {
        ...state,
        navbarLinksResponse: action.payload,
      };
    default:
      return state;
  }
}
