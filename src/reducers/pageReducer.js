import {
  FETCH_PAGES_LIST,
  RECEIVE_PAGES_LIST,
  REMOVE_PAGE,
  CREATE_PAGE,
  UPDATE_PAGE,
  FETCH_PAGE_DATA,
  RECEIVE_PAGE_DATA,
} from "../actions/actionTypes";

const initialState = {
  pages: {},
  currentPage: {},
};

export default function pages(state = initialState, action) {
  let newState;
  switch (action.type) {
    case FETCH_PAGES_LIST:
      return action;
    case RECEIVE_PAGES_LIST:
      newState = { ...state, pages: action.pages };
      return newState;
    case CREATE_PAGE:
      return action;
    case REMOVE_PAGE:
      return action;
    case UPDATE_PAGE:
      return action;
    case FETCH_PAGE_DATA:
      return action;
    case RECEIVE_PAGE_DATA:
      newState = { ...state, currentPage: action.data };
      return newState;
    default:
      return state;
  }
}
