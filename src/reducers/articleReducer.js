import {
  FETCH_ARTICLES_LIST,
  RECEIVE_ARTICLES_LIST,
  REMOVE_ARTICLE,
  CREATE_ARTICLE,
  UPDATE_ARTICLE,
  FETCH_ARTICLE_DATA,
  RECEIVE_ARTICLE_DATA,
} from "../actions/actionTypes";

const initialState = {
  articleList: {},
  currentArticle: {},
};

export default function articles(state = initialState, action) {
  let newState;
  switch (action.type) {
    case FETCH_ARTICLES_LIST:
      return action;
    case RECEIVE_ARTICLES_LIST:
      newState = { ...state, articleList: action.articles };
      return newState;
    case CREATE_ARTICLE:
      return action;
    case REMOVE_ARTICLE:
      return action;
    case UPDATE_ARTICLE:
      return action;
    case FETCH_ARTICLE_DATA:
      return action;
    case RECEIVE_ARTICLE_DATA:
      newState = { ...state, currentArticle: action.data };
      return newState;
    default:
      return state;
  }
}
