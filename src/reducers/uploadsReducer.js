import initialState from "./initialState";
import {
  FETCH_UPLOADS_LIST,
  RECEIVE_UPLOADS_LIST,
} from "../actions/actionTypes";

export default function user(state = initialState.user, action) {
  let newState;
  switch (action.type) {
    case FETCH_UPLOADS_LIST:
      return action;
    case RECEIVE_UPLOADS_LIST:
      newState = { ...state, data: action.data };
      return newState;
    default:
      return state;
  }
}
