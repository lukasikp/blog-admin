import Dashboard from "views/Dashboard/Dashboard";
import Posts from "views/Posts/Posts";
import NewPost from "views/NewPost/NewPost";
import Categories from "views/Categories/Categories";
import Uploads from "views/Uploads/Uploads";
import Pages from "views/Pages/Pages";
import NewPage from "views/NewPage/NewPage";
import Blocks from "views/Blocks/Blocks";

import UserProfile from "views/UserProfile/UserProfile";
// import Icons from "views/Icons/Icons";
// import Notifications from "views/Notifications/Notifications";
// import Upgrade from "views/Upgrade/Upgrade";

const dashboardRoutes = [
  {
    path: "/dashboard",
    name: "Dashboard",
    icon: "pe-7s-graph",
    component: Dashboard,
  },
  {
    path: "/posts",
    name: "Posty",
    icon: "pe-7s-news-paper",
    component: Posts,
  },
  {
    path: "/new-post",
    name: "Nowy post",
    icon: "pe-7s-pen",
    component: NewPost,
  },
  {
    path: "/pages",
    name: "Strony",
    icon: "pe-7s-news-paper",
    component: Pages,
  },
  {
    path: "/new-page",
    name: "Nowa strona",
    icon: "pe-7s-news-paper",
    component: NewPage,
    sidebarHide: true,
  },
  {
    path: "/categories",
    name: "Kategorie",
    icon: "pe-7s-ticket",
    component: Categories,
  },
  {
    path: "/user",
    name: "Profil",
    icon: "pe-7s-user",
    component: UserProfile,
  },
  // { path: "/icons", name: "Icons", icon: "pe-7s-science", component: Icons },
  {
    path: "/uploads",
    name: "Obrazy",
    icon: "pe-7s-photo",
    component: Uploads,
  },
  {
    path: "/blocks",
    name: "Bloki",
    icon: "pe-7s-next-2",
    component: Blocks,
  },
  { redirect: true, path: "/", to: "dashboard", name: "dashboard" },
];

export default dashboardRoutes;
