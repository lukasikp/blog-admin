import React, { Component } from "react";
import { Pager } from "react-bootstrap";

class Pagination extends Component {
  constructor(props) {
    super(props);
  }

  render() {
    return (
      <Pager>
        <Pager.Item
          previous
          disabled={this.props.previous === null ? true : false}
          onClick={
            this.props.previous === null
              ? null
              : () => this.props.paginate(this.props.previous)
          }
        >
          &larr; Previous
        </Pager.Item>
        <Pager.Item
          next
          disabled={this.props.next === null ? true : false}
          onClick={
            this.props.next === null
              ? null
              : () => this.props.paginate(this.props.next)
          }
        >
          Next &rarr;
        </Pager.Item>
      </Pager>
    );
  }
}

export default Pagination;
