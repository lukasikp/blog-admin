import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import { NavItem, Nav } from "react-bootstrap";
import * as userActions from "../../actions/userActions";

class HeaderLinks extends Component {
  render() {
    return (
      <div>
        <Nav>
          <NavItem eventKey={1} href="#">
            <p className="hidden-lg hidden-md">Dashboard</p>
          </NavItem>
        </Nav>
        <Nav pullRight>
          <NavItem onClick={() => this.props.userActions.logoutUser()}>
            Log out
          </NavItem>
        </Nav>
      </div>
    );
  }
}

const styles = {
  logoutButton: {
    padding: "10px 15px",
    margin: "10px 3px",
    color: "#9a9a9a",
  },
};

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}
function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(HeaderLinks);
