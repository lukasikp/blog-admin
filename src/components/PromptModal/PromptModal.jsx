import React from "react";
import { Modal } from "react-bootstrap";
import Button from "../CustomButton/CustomButton.jsx";

const PropmptModal = props => {
  return (
    <Modal show={props.active} onHide={props.onHide}>
      <Modal.Header closeButton>
        <Modal.Title>{props.title}</Modal.Title>
      </Modal.Header>

      <Modal.Footer>
        <Button onClick={props.onHide} bsStyle="default" fill>
          Anuluj
        </Button>
        <Button bsStyle="danger" onClick={props.confirmAction} fill>
          {props.confirmButton}
        </Button>
      </Modal.Footer>
    </Modal>
  );
};

export default PropmptModal;
