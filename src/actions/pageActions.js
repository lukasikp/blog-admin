import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";

const axios = require("axios");
const querystring = require("querystring");

export function receivePagesList(json) {
  return { type: types.RECEIVE_PAGES_LIST, pages: json };
}

export function fetchPagesList(url = `${api_url}/pages?published=false`) {
  return dispatch => {
    return axios.get(url).then(json => dispatch(receivePagesList(json.data)));
  };
}

export function createPage(token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .post(`${api_url}/pages`, data, {
        headers: {
          Authorization: authorization,
          "Content-Type": "application/json",
        },
      })
      .catch(err => console.log(err));
  };
}

export function updatePage(token, data, _id) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .patch(`${api_url}/pages/${_id}`, data, {
        headers: {
          Authorization: authorization,
          "Content-Type": "application/json",
        },
      })
      .catch(err => console.log(err));
  };
}

export function removePage(_id, token) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    const url = `${api_url}/pages/${_id}`;
    return axios
      .delete(url, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => console.log(response))
      .then(() => dispatch(fetchPagesList()));
  };
}

export function receivePageData(json) {
  return { type: types.RECEIVE_PAGE_DATA, data: json };
}

export function fetchPageData(id) {
  return dispatch => {
    return axios
      .get(`${api_url}/pages/${id}`)
      .then(json => dispatch(receivePageData(json.data)));
  };
}
