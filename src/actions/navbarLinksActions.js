import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";

const axios = require("axios");
const querystring = require("querystring");

const urlParam = "/navbar-links";

export function receiveNavbarLinks(json) {
  return { type: types.RECEIVE_NAVBAR_LIST, payload: json };
}

export function fetchNavbarLinks() {
  return dispatch => {
    return axios
      .get(api_url + urlParam)
      .then(json => dispatch(receiveNavbarLinks(json.data)));
  };
}

export function createNavbarLinks(token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .post(api_url + urlParam, querystring.stringify(data), {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => dispatch(navbarLinksServerResponse(response)))
      .then(() => dispatch(fetchNavbarLinks()))
      .catch(response =>
        dispatch(navbarLinksServerResponse(response.response))
      );
  };
}

export function editNavbarLinks(_id, token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .patch(`${api_url}${urlParam}/${_id}`, querystring.stringify(data), {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => dispatch(navbarLinksServerResponse(response)))
      .then(() => dispatch(fetchNavbarLinks()))
      .catch(response =>
        dispatch(navbarLinksServerResponse(response.response))
      );
  };
}

export function removeNavbarLinks(_id, token) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    const url = `${api_url}${urlParam}/${_id}`;
    return axios
      .delete(url, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => {
        response === undefined
          ? dispatch(navbarLinksServerResponse({ message: "Link deleted" }))
          : dispatch(navbarLinksServerResponse(response));
      })
      .then(() => dispatch(fetchNavbarLinks()))
      .catch(response =>
        dispatch(navbarLinksServerResponse(response.response))
      );
  };
}

export function navbarLinksServerResponse(response) {
  return {
    type: types.NAVBAR_LINKS_SERVER_RESPONSE,
    payload: { ...response, id: Math.random() },
  };
}
