import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";
import axios from "axios";

export function receiveUploadsList(json) {
  return { type: types.RECEIVE_UPLOADS_LIST, data: json };
}

export function fetchUploadsList(url, token) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .get(url, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(json => dispatch(receiveUploadsList(json.data)));
  };
}
