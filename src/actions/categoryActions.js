import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";

const axios = require("axios");
const querystring = require("querystring");

const urlParam = "/categories";

export function receiveCategoryList(json) {
  return { type: types.RECEIVE_CATEGORY_LIST, categories: json };
}

export function fetchCategoryList() {
  return dispatch => {
    return axios
      .get(api_url + urlParam)
      .then(json => dispatch(receiveCategoryList(json.data)));
  };
}

export function createCategory(token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .post(api_url + urlParam, querystring.stringify(data), {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => dispatch(categoryServerResponse(response)))
      .then(() => dispatch(fetchCategoryList()))
      .catch(response => dispatch(categoryServerResponse(response.response)));
  };
}

export function editCategory(_id, token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .patch(`${api_url}${urlParam}/${_id}`, querystring.stringify(data), {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => dispatch(categoryServerResponse(response)))
      .then(() => dispatch(fetchCategoryList()))
      .catch(response => dispatch(categoryServerResponse(response.response)));
  };
}

export function removeCategory(_id, token) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    const url = `${api_url}${urlParam}/${_id}`;
    return axios
      .delete(url, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => {
        response === undefined
          ? dispatch(categoryServerResponse({ message: "Product deleted" }))
          : dispatch(categoryServerResponse(response));
      })
      .then(() => dispatch(fetchCategoryList()))
      .catch(response => dispatch(categoryServerResponse(response.response)));
  };
}

export function categoryServerResponse(response) {
  return {
    type: types.CATEGORY_SERVER_RESPONSE,
    payload: { ...response, id: Math.random() },
  };
}
