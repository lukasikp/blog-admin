import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";

const axios = require("axios");

export function receiveUserToken(json) {
  return { type: types.RECEIVE_USER_TOKEN, user: json };
}

export function loginUser(postData) {
  return dispatch => {
    return fetch(api_url + "/user/login", {
      method: "POST",
      headers: {
        Accept: "application/json",
        "Content-Type": "application/json",
      },
      body: JSON.stringify(postData),
    })
      .then(response => response.json())
      .then(json => dispatch(receiveUserToken(json)));
  };
}

export function receiveUserData(json) {
  return { type: types.RECEIVE_USER_DATA, data: json };
}

export function fetchUserData(param) {
  return dispatch => {
    return axios
      .get(`${api_url}/user/${param}`)
      .then(json => dispatch(receiveUserData(json.data.result)));
  };
}

export function updateUserData(param, token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .patch(`${api_url}/user/${param}`, data, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(json => dispatch(fetchUserData(param)));
  };
}

export function logoutUser() {
  return { type: types.LOGOUT_USER, data: {} };
}
