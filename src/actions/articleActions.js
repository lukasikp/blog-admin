import * as types from "./actionTypes";
import { api_url } from "../constans/index.js";

const axios = require("axios");
const querystring = require("querystring");

export function receiveArticlesList(json) {
  return { type: types.RECEIVE_ARTICLES_LIST, articles: json };
}

export function fetchArticlesList(url = `${api_url}/articles?published=false`) {
  return dispatch => {
    return axios
      .get(url)
      .then(json => dispatch(receiveArticlesList(json.data)));
  };
}

export function createArticle(token, data) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .post(`${api_url}/articles`, data, {
        headers: {
          Authorization: authorization,
          "Content-Type": "form-data",
        },
      })
      .catch(err => console.log(err));
  };
}

export function updateArticle(token, data, _id) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    return axios
      .patch(`${api_url}/articles/${_id}`, data, {
        headers: {
          Authorization: authorization,
          "Content-Type": "application/json",
        },
      })
      .catch(err => console.log(err));
  };
}

export function removeArticle(_id, token) {
  return dispatch => {
    const authorization = `Bearer ${token}`;
    const url = `${api_url}/articles/${_id}`;
    return axios
      .delete(url, {
        headers: {
          Authorization: authorization,
        },
      })
      .then(response => console.log(response))
      .then(() => dispatch(fetchArticlesList()));
  };
}

export function receiveArticleData(json) {
  return { type: types.RECEIVE_ARTICLE_DATA, data: json };
}

export function fetchArticleData(id) {
  return dispatch => {
    return axios
      .get(`${api_url}/articles/${id}`)
      .then(json => dispatch(receiveArticleData(json.data)));
  };
}
