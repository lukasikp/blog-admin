
import { createBrowserHistory } from 'history';
import {createStore, compose, applyMiddleware} from 'redux';
import { connectRouter, routerMiddleware } from 'connected-react-router'
import rootReducer from '../reducers/rootReducer';
import thunk from 'redux-thunk';
import { persistStore, persistReducer } from 'redux-persist';
import storage from 'redux-persist/lib/storage';

const persistConfig = {
  key: 'root',
  storage,
}

const persistedReducer = persistReducer(persistConfig, rootReducer)

export const history = createBrowserHistory()

export default function configureStore(){
  let store = createStore(
    connectRouter(history)(persistedReducer),
    compose(
      applyMiddleware(
        routerMiddleware(history), thunk 
      ),
    ),
  )

  let persistor = persistStore(store)
  return {store, persistor}
}
