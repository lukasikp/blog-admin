import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as userActions from "../../actions/userActions";

import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
} from "react-bootstrap";

import { Card } from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import { UserCard } from "components/UserCard/UserCard.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import { api_url } from "../../constans/index.js";

import { styles } from "./UserProfile.styles";

const axios = require("axios");

class UserProfile extends Component {
  componentDidMount() {
    this.props.userActions.fetchUserData(this.props.user.userId);
  }

  handleSubmit = e => {
    e.preventDefault();
    const formElements = Array.from(e.target.elements);
    const user = this.props.user;
    const submitData = formElements.map(element => {
      return {
        propName: element.name,
        value: element.value,
      };
    });
    this.props.userActions.updateUserData(user.userId, user.token, submitData);
  };

  saveImage = bodyFormData => {
    const request = axios.post(`${api_url}/uploads`, bodyFormData, {
      headers: {
        Authorization: `Bearer ${this.props.user.token}`,
      },
    });
    return request;
  };

  updateBgPic = () => {
    const bodyFormData = new FormData();
    bodyFormData.append("image", this.bgImage.files[0]);
    const user = this.props.user;
    this.saveImage(bodyFormData)
      .then(response => {
        const data = [
          {
            propName: "bgImage",
            value: `${response.data.uploaded.result.image}`,
          },
        ];
        this.props.userActions
          .updateUserData(user.userId, user.token, data)
          .then(this.props.userActions.fetchUserData(this.props.user.userId));
      })
      .catch(err => console.log(err));
  };

  updatePic = () => {
    const bodyFormData = new FormData();
    bodyFormData.append("image", this.image.files[0]);
    const user = this.props.user;
    this.saveImage(bodyFormData)
      .then(response => {
        const data = [
          {
            propName: "image",
            value: `${response.data.uploaded.result.image}`,
          },
        ];
        this.props.userActions
          .updateUserData(user.userId, user.token, data)
          .then(this.props.userActions.fetchUserData(this.props.user.userId));
      })
      .catch(err => console.log(err));
  };

  render() {
    const user = this.props.user.data;
    return (
      <div className="content">
        <Grid fluid>
          {user !== undefined ? (
            <Row>
              <Col md={8}>
                <Card
                  title="Twój profil"
                  content={
                    <form onSubmit={this.handleSubmit}>
                      <FormInputs
                        ncols={["col-md-6", "col-md-6"]}
                        proprieties={[
                          {
                            label: "Imię",
                            name: "name",
                            type: "text",
                            bsClass: "form-control",
                            placeholder: "Imię",
                            defaultValue: user.name || "",
                          },
                          {
                            label: "Nazwisko",
                            name: "surname",
                            type: "text",
                            bsClass: "form-control",
                            placeholder: "Nazwisko",
                            defaultValue: user.surname || "",
                          },
                        ]}
                      />
                      <FormInputs
                        ncols={["col-md-6", "col-md-6"]}
                        proprieties={[
                          {
                            label: "Pseudonim",
                            name: "nick",
                            type: "text",
                            bsClass: "form-control",
                            placeholder: "Pseudonim",
                            defaultValue: user.nick || "",
                          },
                          {
                            label: "E-mail",
                            name: "email",
                            type: "email",
                            bsClass: "form-control",
                            placeholder: "E-mail",
                            defaultValue: user.email || "",
                            disabled: true,
                          },
                        ]}
                      />
                      <Row>
                        <Col md={12}>
                          <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>O mnie</ControlLabel>
                            <FormControl
                              rows="5"
                              componentClass="textarea"
                              name="description"
                              bsClass="form-control"
                              placeholder="Kilka słów o mnie"
                              defaultValue={user.description || ""}
                            />
                          </FormGroup>
                        </Col>
                        <Col md={12}>
                          <Col md={6}>
                            <FormInputs
                              ncols={["col-md-12"]}
                              proprieties={[
                                {
                                  label: "Zdjęcie",
                                  type: "file",
                                  bsClass: "form-control",
                                  defaultValue: user.image || "",
                                  inputRef: ref => {
                                    this.image = ref;
                                  },
                                },
                              ]}
                            />
                            <Button
                              style={styles.smallButton}
                              fill
                              onClick={this.updatePic}
                            >
                              Zapisz zdjęcie
                            </Button>
                          </Col>
                          <Col md={6}>
                            <FormInputs
                              ncols={["col-md-12"]}
                              proprieties={[
                                {
                                  label: "Zdjęcie w tle",
                                  type: "file",
                                  bsClass: "form-control",
                                  defaultValue: user.bgImage || "",
                                  inputRef: ref => {
                                    this.bgImage = ref;
                                  },
                                },
                              ]}
                            />
                            <Button
                              style={styles.smallButton}
                              fill
                              onClick={this.updateBgPic}
                            >
                              Zapisz zdjęcie
                            </Button>
                          </Col>
                        </Col>
                      </Row>
                      <Button bsStyle="info" pullRight fill type="submit">
                        Zapisz zmiany
                      </Button>
                      <div className="clearfix" />
                    </form>
                  }
                />
              </Col>
              <Col md={4}>
                <UserCard
                  bgImage={`${api_url}/${user.bgImage}` || ""}
                  avatar={`${api_url}/${user.image}` || ""}
                  name={`${user.name || ""} ${user.surname || ""}`}
                  userName={user.nick || ""}
                  description={user.description || ""}
                />
              </Col>
            </Row>
          ) : null}
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(UserProfile);
