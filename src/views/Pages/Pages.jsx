import React, { Component } from "react";
import { NavLink, Route, Switch, Redirect } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import moment from "moment";

import * as pageActions from "../../actions/pageActions";
import { Grid, Row, Col, Table, MenuItem } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import Pagination from "components/Pagination/Pagination.jsx";
import PromptModal from "components/PromptModal/PromptModal";

import { styles } from "./Pages.styles";

const postThArray = ["Nazwa", "Url", "Data publikacji"];

class Pages extends Component {
  state = {
    modalActive: false,
    focusPost: "",
    focusPostId: "",
  };
  componentDidMount() {
    this.props.pageActions.fetchPagesList();
  }

  removePage = () => {
    this.props.pageActions.removePage(
      this.state.focusPostId,
      this.props.user.token
    );
    this.setState({
      modalActive: false,
      focusPost: "",
      focusPostId: "",
    });
  };

  paginate = url => {
    this.props.pageActions.fetchPagesList(url);
  };
  render() {
    const { pages } = this.props.pages;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={styles.container}>
                    <div style={styles.buttonContainer}>
                      <NavLink
                        to="/new-page"
                        className="nav-link"
                        activeClassName="active"
                      >
                        <Button bsStyle="success" fill pullRight>
                          Utwórz stronę
                        </Button>
                      </NavLink>
                    </div>
                    <Table striped hover>
                      <thead>
                        <tr>
                          {postThArray.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                      </thead>
                      <tbody>
                        {pages.result === undefined ||
                        pages.result.length === 0 ? (
                          <tr>
                            <td>Nie ma jeszcze utworzonych stron</td>
                            <td />
                          </tr>
                        ) : (
                          pages.result.map((prop, key) => {
                            return (
                              <tr key={key}>
                                <td>{prop.name}</td>
                                <td>{prop.url}</td>
                                <td>
                                  {prop.publishedAt ? (
                                    moment(prop.publishedAt)
                                      .subtract(10, "days")
                                      .calendar()
                                  ) : (
                                    <span style={styles.span}>
                                      nieopublikowana
                                    </span>
                                  )}
                                </td>
                                <td>
                                  <NavLink
                                    to={`/new-page/${prop._id}`}
                                    className="nav-link"
                                    activeClassName="active"
                                  >
                                    <span
                                      className="pe-7s-note"
                                      style={styles.icon}
                                    />
                                  </NavLink>
                                  <span
                                    onClick={() =>
                                      this.setState({
                                        modalActive: true,
                                        focusPost: prop.name,
                                        focusPostId: prop._id,
                                      })
                                    }
                                    className="pe-7s-trash"
                                    style={styles.icon}
                                  />
                                </td>
                              </tr>
                            );
                          })
                        )}
                      </tbody>
                    </Table>
                    {pages.total > pages.count ? (
                      <Pagination
                        count={pages.count}
                        total={pages.total}
                        next={pages.next}
                        previous={pages.previous}
                        paginate={this.paginate}
                      />
                    ) : null}
                  </div>
                }
              />
            </Col>
          </Row>
          <PromptModal
            active={this.state.modalActive}
            onHide={() => this.setState({ modalActive: false })}
            name={`Na pewno usunąć post: ${this.state.focusPost}?`}
            confirmButton={"Usuń post"}
            confirmAction={() => this.removePage()}
          />
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    pages: state.pages,
    categories: state.categories,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Pages);
