export const styles = {
  icon: {
    width: "30px",
    fontSize: "18px",
    cursor: "pointer",
  },
  container: {
    margin: 20,
  },
  buttonContainer: {
    marginBottom: 20,
  },
  span: {
    color: "#d5d5d5",
    fontSize: "12px",
  },
};
