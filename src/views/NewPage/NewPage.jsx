import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import slug from "slug";

import * as pageActions from "../../actions/pageActions";
import { Grid, Row, Col } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { api_url } from "../../constans/index.js";

import { styles } from "./NewPage.style";
import { inputs } from "./NewPage.properties";

const axios = require("axios");

class NewPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      pageId: "",
      name: "",
      editorState: EditorState.createEmpty(),
      published: false,
      saved: false,
      publishedAt: null,
    };
  }

  componentDidMount() {
    const pageId = this.getPageId();
    pageId !== "new-page" ? this.buildInitState(pageId) : null;
  }

  getPageId = () => {
    const path = window.location.href.split("/");
    return path[path.length - 1];
  };

  buildInitState = id => {
    this.props.pageActions.fetchPageData(id).then(response => {
      const page = this.props.pages.currentPage.result;
      this.setState({
        pageId: id,
        name: page.name,
        editorState: EditorState.createWithContent(
          convertFromRaw(JSON.parse(page.content))
        ),
        saved: true,
        published: page.publishedAt ? true : false,
      });
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState,
    });
    // const rawState = JSON.stringify(
    //   convertToRaw(editorState.getCurrentContent())
    // );
  };

  uploadFileFromWysiwyg = file => {
    const authorization = `Bearer ${this.props.user.token}`;
    const bodyFormData = new FormData();
    bodyFormData.append("image", file);
    const request = axios.post(`${api_url}/uploads`, bodyFormData, {
      headers: {
        Authorization: authorization,
      },
    });
    return request
      .then(response => {
        return {
          data: { link: `${api_url}/${response.data.uploaded.result.image}` },
        };
      })
      .catch(err => console.log(err));
  };

  buildFormData = publishArticle => {
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = {
      name: this.state.name,
      url: slug(this.state.name),
      content: rawState,
      publishedAt: publishArticle ? new Date() : null,
    };
    return bodyFormData;
  };

  submitForm = e => {
    e.preventDefault();
    const bodyFormData = this.buildFormData(false);
    this.props.pageActions
      .createPage(this.props.user.token, bodyFormData)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );

    this.setState({ saved: true });
  };

  patchForm = e => {
    e.preventDefault();
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = [
      {
        propName: "name",
        value: this.state.name,
      },
      {
        propName: "content",
        value: rawState,
      },
    ];

    this.props.pageActions
      .updatePage(this.props.user.token, bodyFormData, this.state.pageId)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );

    this.setState({ saved: true });
  };

  publishPage = e => {
    e.preventDefault();
    !this.state.saved ? this.publishUnsavedPage() : this.publishSavedPage();
    this.setState({ saved: true, published: true });
  };

  publishUnsavedPage = () => {
    const bodyFormData = this.buildFormData(true);
    this.props.articleActions
      .createPage(this.props.user.token, bodyFormData)
      .then(response =>
        response
          ? this.props.showNotification(
              response.status,
              response.data.userMessage
            )
          : null
      );
  };

  publishSavedPage = () => {
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = [
      {
        propName: "name",
        value: this.state.name,
      },
      {
        propName: "content",
        value: rawState,
      },
      {
        propName: "publishedAt",
        value: new Date(),
      },
      {
        propName: "published",
        value: true,
      },
    ];

    this.props.pageActions
      .updatePage(this.props.user.token, bodyFormData, this.state.pageId)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  render() {
    const { editorState } = this.state;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={styles.formContainer}>
                    <form onSubmit={this.handleSubmit}>
                      <div style={styles.buttonContainer}>
                        <NavLink
                          to="/pages"
                          className="nav-link"
                          activeClassName="active"
                        >
                          <Button
                            style={styles.buttonStyle}
                            pullRight
                            fill
                            type="button"
                          >
                            Zamknij <i className="fa fa-times" />
                          </Button>
                        </NavLink>
                        <Button
                          bsStyle="success"
                          style={styles.buttonStyle}
                          pullRight
                          fill
                          type="submit"
                          onClick={
                            this.state.saved ? this.patchForm : this.submitForm
                          }
                        >
                          {this.state.saved ? "Zapisz zmiany" : "Zapisz stronę"}{" "}
                          <i className="fa fa-save" />
                        </Button>
                        {!this.state.published ? (
                          <Button
                            bsStyle="info"
                            style={styles.buttonStyle}
                            pullRight
                            fill
                            type="submit"
                            onClick={this.publishPage}
                          >
                            Opublikuj <i className="fa fa-check" />
                          </Button>
                        ) : null}
                      </div>
                      <FormInputs
                        ncols={["col-md-12"]}
                        proprieties={[
                          {
                            ...inputs.name,
                            inputRef: ref => {
                              this.name = ref;
                            },
                            value: this.state.name,
                            onChange: this.handleChange,
                          },
                        ]}
                      />
                      <Editor
                        editorState={editorState}
                        editorStyle={styles.wysiwygStyle}
                        toolbarStyle={styles.inputStyle}
                        onEditorStateChange={this.onEditorStateChange}
                        toolbar={{
                          image: {
                            urlEnabled: true,
                            uploadEnabled: true,
                            uploadCallback: this.uploadFileFromWysiwyg,
                          },
                        }}
                      />
                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    pages: state.pages,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    pageActions: bindActionCreators(pageActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPage);
