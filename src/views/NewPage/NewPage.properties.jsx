export const inputs = {
  name: {
    label: "Nazwa strony",
    name: "name",
    type: "text",
    bsClass: "form-control",
    required: true,
  },
};
