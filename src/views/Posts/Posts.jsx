import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import moment from "moment";

import * as articleActions from "../../actions/articleActions";
import { Grid, Row, Col, Table, MenuItem } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import Pagination from "components/Pagination/Pagination.jsx";
import PromptModal from "components/PromptModal/PromptModal";

import { styles } from "./Posts.styles";

const postThArray = ["Tytuł", "Kategoria", "Autor", "Data publikacji"];

class Posts extends Component {
  state = {
    modalActive: false,
    focusPost: "",
    focusPostId: "",
  };
  componentDidMount() {
    this.props.articleActions.fetchArticlesList();
  }

  removeArticle = () => {
    this.props.articleActions.removeArticle(
      this.state.focusPostId,
      this.props.user.token
    );
    this.setState({
      modalActive: false,
      focusPost: "",
      focusPostId: "",
    });
  };

  paginate = url => {
    this.props.articleActions.fetchArticlesList(url);
  };
  render() {
    const { articleList } = this.props.articles;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={styles.container}>
                    <div style={styles.buttonContainer}>
                      <NavLink
                        to="/new-post"
                        className="nav-link"
                        activeClassName="active"
                      >
                        <Button bsStyle="success" fill pullRight>
                          Nowy post
                        </Button>
                      </NavLink>
                    </div>
                    <Table striped hover>
                      <thead>
                        <tr>
                          {postThArray.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                      </thead>
                      <tbody>
                        {articleList.result !== undefined &&
                          articleList.result.map((prop, key) => {
                            return (
                              <tr key={key}>
                                <td>{prop.title}</td>
                                <td>
                                  {prop.category ? prop.category.name : ""}
                                </td>
                                <td>Ala</td>
                                <td>
                                  {prop.publishedAt ? (
                                    moment(prop.publishedAt)
                                      .subtract(10, "days")
                                      .calendar()
                                  ) : (
                                    <span style={styles.span}>
                                      nieopublikowany
                                    </span>
                                  )}
                                </td>
                                <td>
                                  <NavLink
                                    to={`/new-post/${prop._id}`}
                                    className="nav-link"
                                    activeClassName="active"
                                  >
                                    <span
                                      className="pe-7s-note"
                                      style={styles.icon}
                                    />
                                  </NavLink>
                                  <span
                                    onClick={() =>
                                      this.setState({
                                        modalActive: true,
                                        focusPost: prop.title,
                                        focusPostId: prop._id,
                                      })
                                    }
                                    className="pe-7s-trash"
                                    style={styles.icon}
                                  />
                                </td>
                              </tr>
                            );
                          })}
                      </tbody>
                    </Table>
                    {articleList.total > articleList.count ? (
                      <Pagination
                        count={articleList.count}
                        total={articleList.total}
                        next={articleList.next}
                        previous={articleList.previous}
                        paginate={this.paginate}
                      />
                    ) : null}
                  </div>
                }
              />
            </Col>
          </Row>
          <PromptModal
            active={this.state.modalActive}
            onHide={() => this.setState({ modalActive: false })}
            title={`Na pewno usunąć post: ${this.state.focusPost}?`}
            confirmButton={"Usuń post"}
            confirmAction={() => this.removeArticle()}
          />
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    articles: state.articles,
    categories: state.categories,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Posts);
