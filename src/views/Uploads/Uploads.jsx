import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";

import * as uploadsActions from "../../actions/uploadsActions";

import { Grid, Row, Col } from "react-bootstrap";

import Card from "components/Card/Card";
import PromptModal from "components/PromptModal/PromptModal";
import Button from "components/CustomButton/CustomButton.jsx";
import { iconsArray } from "variables/Variables.jsx";
import { api_url } from "../../constans/index.js";
import axios from "axios";

import { styles } from "./Uploads.styles";

class Uploads extends Component {
  constructor(props) {
    super(props);
    this.state = {
      modalActive: false,
      focusImage: "",
      currentUrl: `${api_url}/uploads/`,
      backUrl: "",
    };
  }
  componentDidMount() {
    this.props.uploadsActions.fetchUploadsList(
      this.state.currentUrl,
      this.props.user.token
    );
  }

  deleteImage(url) {
    const request = axios
      .delete(url, {
        headers: {
          Authorization: `Bearer ${this.props.user.token}`,
        },
      })
      .then(response => {
        this.props.uploadsActions.fetchUploadsList(
          this.state.currentUrl,
          this.props.user.token
        );
      });
  }

  removeImage = () => {
    const url = `${this.state.currentUrl}/${this.state.focusImage}`;
    this.deleteImage(url);
    this.setState({
      modalActive: false,
      focusImage: "",
    });
  };

  getFolderData = url => {
    const backUrl = this.state.currentUrl;
    this.setState({
      currentUrl: url,
      backUrl: backUrl,
    });
    this.props.uploadsActions.fetchUploadsList(
      `${url}/`,
      this.props.user.token
    );
  };

  getBackFolderData = url => {
    const backUrl = this.getBackUrl(url);
    this.setState({
      currentUrl: url,
      backUrl: backUrl === api_url ? "" : backUrl,
    });
    this.props.uploadsActions.fetchUploadsList(
      `${url}/`,
      this.props.user.token
    );
  };

  getBackUrl = url => {
    const urlNew =
      url[url.length - 1] === "/" ? url.slice(0, url.length - 1) : url;
    let urlArray = urlNew.split("/").slice(0, -1);
    let backUrl = urlArray.join("/");
    return backUrl;
  };

  renderContainer = link => {
    const name = link
      .split("/")
      .filter(i => i !== "")
      .pop();
    return name.length <= 4 ? this.createFolder(name) : this.createImage(name);
  };

  createFolder = name => {
    return (
      <div
        key={name}
        onClick={() => this.getFolderData(`${this.state.currentUrl}/${name}`)}
        style={styles.folder}
      >
        <i className=" pe-7s-albums" color="#9A9A9A" />
        <p>{name}</p>
      </div>
    );
  };
  createImage = name => {
    return (
      <Col lg={2} md={3} sm={4} xs={6} key={name}>
        <div style={styles.imageBox}>
          <img
            src={`${this.state.currentUrl}/${name}`}
            alt=""
            style={styles.image}
          />
          <p style={styles.imageName}>{name}</p>
          <p
            style={styles.smallButton}
            onClick={() =>
              this.setState({
                modalActive: true,
                focusImage: name,
              })
            }
          >
            Usuń zdjęcie
          </p>
        </div>
      </Col>
    );
  };
  render() {
    const data = this.props.uploads.data.result;
    const dataList = this.props.uploads.data.result;
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                title="Wgrane zdjęcia"
                ctAllIcons
                category={
                  <span>
                    Tutaj można usuwać niepotrzebne zdjęcia, jeśli jest się
                    pewnym, że są nieużywane
                  </span>
                }
                content={
                  <Row>
                    {this.state.backUrl !== "" && (
                      <Button
                        fill
                        style={styles.button}
                        onClick={() =>
                          this.getBackFolderData(`${this.state.backUrl}`)
                        }
                      >
                        wróć
                      </Button>
                    )}
                    <div style={styles.flexRow}>
                      {dataList.map(item => this.renderContainer(item))}
                    </div>
                  </Row>
                }
              />
            </Col>
          </Row>

          <PromptModal
            active={this.state.modalActive}
            onHide={() => this.setState({ modalActive: false })}
            title={`Na pewno usunąć zdjęcie ${this.state.focusImage}?`}
            confirmButton={"Usuń zdjęcie"}
            confirmAction={() => this.removeImage()}
          />
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    uploads: state.uploads,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    uploadsActions: bindActionCreators(uploadsActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Uploads);
