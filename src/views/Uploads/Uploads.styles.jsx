export const styles = {
  imageBox: {
    borderWidth: 1,
    borderColor: "#e5e5e5",
    borderRadius: "6px",
    height: "210",
    overflow: "hidden",
  },
  image: {
    maxWidth: "100%",
    maxHeight: "160px",
  },
  imageName: {
    fontSize: 10,
    marginBottom: 0,
  },
  smallButton: {
    textDecoration: "underline",
    color: "#1DC7EA",
    fontSize: 10,
    marginTop: 0,
    cursor: "pointer",
  },
  flexRow: {
    display: "flex",
    justifyContent: "flex-start",
    flexWrap: "wrap",
  },
  folder: {
    marginLeft: 30,
    marginRight: 30,
    marginBottom: 15,
    cursor: "pointer",
    textAlign: "center",
  },
  button: {
    marginLeft: 15,
    marginBottom: 15,
  },
};
