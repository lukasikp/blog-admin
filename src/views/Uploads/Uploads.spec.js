import expect from "expect";

const url = "http://trendnakulture.pl/uploads/2019/3/";
const getBackUrl = url => {
  const urlNew =
    url[url.length - 1] === "/" ? url.slice(0, url.length - 1) : url;
  let urlArray = urlNew.split("/").slice(0, -1);
  let backUrl = urlArray.join("/");
  return backUrl;
};

describe("empty", () => {
  it("should work", () => {
    expect(getBackUrl(url)).toEqual("http://trendnakulture.pl/uploads/2019");
  });
});
