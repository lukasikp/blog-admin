import React, { Component } from "react";
import { NavLink } from "react-router-dom";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import slug from "slug";

import * as articleActions from "../../actions/articleActions";
import * as categoryActions from "../../actions/categoryActions";
import {
  Grid,
  Row,
  Col,
  FormGroup,
  ControlLabel,
  FormControl,
} from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

import { Editor } from "react-draft-wysiwyg";
import { EditorState, convertToRaw, convertFromRaw } from "draft-js";
import "react-draft-wysiwyg/dist/react-draft-wysiwyg.css";

import { api_url } from "../../constans/index.js";

import { style } from "variables/Variables.jsx";
import { styles } from "./NewPost.style";
import { inputs } from "./NewPost.properties";

const axios = require("axios");

class NewPost extends Component {
  constructor(props) {
    super(props);
    this.state = {
      articleId: "",
      published: false,
      saved: false,
      editorState: EditorState.createEmpty(),
      title: "",
      lead: "",
      category: "",
      articleImage: "",
      tags: "",
      publishedAt: null,
    };
  }

  componentDidMount() {
    const articleId = this.getArticleId();
    articleId !== "new-post" ? this.buildInitState(articleId) : null;
    this.props.categoryActions.fetchCategoryList();
  }

  getArticleId = () => {
    const path = window.location.href.split("/");
    return path[path.length - 1];
  };

  buildInitState = id => {
    this.props.articleActions.fetchArticleData(id).then(response => {
      const article = this.props.articles.currentArticle.result;
      this.setState({
        articleId: id,
        title: article.title,
        lead: article.lead,
        category: article.category !== null ? article.category._id : "",
        articleImage: article.articleImage,
        editorState: EditorState.createWithContent(
          convertFromRaw(JSON.parse(article.content))
        ),
        tags: article.tags,
        saved: true,
        published: article.publishedAt ? true : false,
      });
    });
  };

  onEditorStateChange = editorState => {
    this.setState({
      editorState,
    });
    // const rawState = JSON.stringify(
    //   convertToRaw(editorState.getCurrentContent())
    // );
  };

  uploadFileFromWysiwyg = file => {
    const authorization = `Bearer ${this.props.user.token}`;
    const bodyFormData = new FormData();
    bodyFormData.append("image", file);
    const request = axios.post(`${api_url}/uploads`, bodyFormData, {
      headers: {
        Authorization: authorization,
      },
    });
    return request
      .then(response => {
        return {
          data: { link: `${api_url}/${response.data.uploaded.result.image}` },
        };
      })
      .catch(err => console.log(err));
  };

  buildFormData = publishArticle => {
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = new FormData();
    bodyFormData.append("title", this.title.value);
    bodyFormData.append("lead", this.lead.value);
    bodyFormData.append("url", slug(this.title.value));
    bodyFormData.append("content", rawState);
    bodyFormData.append("categoryId", this.category.value);
    bodyFormData.append(
      "articleImage",
      this.articleImage.files[0],
      this.articleImage.files[0].name
    );
    publishArticle ? bodyFormData.append("publishedAt", new Date()) : null;
    return bodyFormData;
  };

  submitForm = e => {
    e.preventDefault();

    const bodyFormData = this.buildFormData(false);

    this.props.articleActions
      .createArticle(this.props.user.token, bodyFormData)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );

    this.setState({ saved: true });
  };

  patchForm = e => {
    e.preventDefault();
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = [
      {
        propName: "title",
        value: this.title.value,
      },
      {
        propName: "lead",
        value: this.lead.value,
      },
      {
        propName: "categoryId",
        value: this.category.value,
      },
      {
        propName: "content",
        value: rawState,
      },
      {
        propName: "articleImage",
        value: this.state.articleImage,
      },
    ];

    this.props.articleActions
      .updateArticle(this.props.user.token, bodyFormData, this.state.articleId)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );

    this.setState({ saved: true });
  };

  publishArticle = e => {
    e.preventDefault();
    !this.state.saved
      ? this.publishUnsavedArticle()
      : this.publishSavedArticle();
    this.setState({ saved: true, published: true });
  };

  publishUnsavedArticle = () => {
    const bodyFormData = this.buildFormData(true);
    this.props.articleActions
      .createArticle(this.props.user.token, bodyFormData)
      .then(response =>
        response
          ? this.props.showNotification(
              response.status,
              response.data.userMessage
            )
          : null
      );
  };

  publishSavedArticle = () => {
    const rawState = JSON.stringify(
      convertToRaw(this.state.editorState.getCurrentContent())
    );
    const bodyFormData = [
      {
        propName: "title",
        value: this.title.value,
      },
      {
        propName: "lead",
        value: this.lead.value,
      },
      {
        propName: "categoryId",
        value: this.category.value,
      },
      {
        propName: "content",
        value: rawState,
      },
      {
        propName: "articleImage",
        value: this.state.articleImage,
      },
      {
        propName: "publishedAt",
        value: new Date(),
      },
      {
        propName: "published",
        value: true,
      },
    ];

    this.props.articleActions
      .updateArticle(this.props.user.token, bodyFormData, this.state.articleId)
      .then(response =>
        this.props.showNotification(response.status, response.data.userMessage)
      );
  };

  handleChange = ({ target: { name, value } }) => {
    this.setState({ [name]: value });
  };

  changArticleImage = () => {
    this.uploadFileFromWysiwyg(this.changeImage.files[0]).then(result => {
      const urlImage = result.data.link.replace(`${api_url}/`, "");
      this.setState({ articleImage: urlImage });
      const bodyFormData = [
        {
          propName: "articleImage",
          value: this.state.articleImage,
        },
      ];

      this.props.articleActions
        .updateArticle(
          this.props.user.token,
          bodyFormData,
          this.state.articleId
        )
        .then(response =>
          this.props.showNotification(
            response.status,
            response.data.userMessage
          )
        );
    });
  };

  render() {
    const { editorState } = this.state;
    const categories =
      this.props.categories.categoryList !== undefined
        ? this.props.categories.categoryList.result
        : [];
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div style={styles.formContainer}>
                    <form onSubmit={this.handleSubmit}>
                      <div style={styles.buttonContainer}>
                        <NavLink
                          to="/posts"
                          className="nav-link"
                          activeClassName="active"
                        >
                          <Button
                            style={styles.buttonStyle}
                            pullRight
                            fill
                            type="button"
                          >
                            Zamknij <i className="fa fa-times" />
                          </Button>
                        </NavLink>
                        <Button
                          bsStyle="success"
                          style={styles.buttonStyle}
                          pullRight
                          fill
                          type="submit"
                          onClick={
                            this.state.saved ? this.patchForm : this.submitForm
                          }
                        >
                          {this.state.saved ? "Zapisz zmiany" : "Zapisz post"}{" "}
                          <i className="fa fa-save" />
                        </Button>
                        {!this.state.published ? (
                          <Button
                            bsStyle="info"
                            style={styles.buttonStyle}
                            pullRight
                            fill
                            type="submit"
                            onClick={this.publishArticle}
                          >
                            Opublikuj <i className="fa fa-check" />
                          </Button>
                        ) : null}
                        <Button
                          style={styles.buttonStyle}
                          pullRight
                          fill
                          type="submit"
                          onClick={this.submitForm}
                        >
                          Podgląd
                        </Button>
                        <p>publikowane jako:</p>
                      </div>
                      <FormInputs
                        ncols={["col-md-12"]}
                        proprieties={[
                          {
                            ...inputs.title,
                            inputRef: ref => {
                              this.title = ref;
                            },
                            value: this.state.title,
                            onChange: this.handleChange,
                          },
                        ]}
                      />
                      <Row>
                        <Col md={12}>
                          <FormGroup controlId="formControlsTextarea">
                            <ControlLabel>Lead</ControlLabel>
                            <FormControl
                              rows="5"
                              inputRef={ref => {
                                this.lead = ref;
                              }}
                              onChange={this.handleChange}
                              componentClass="textarea"
                              name="lead"
                              bsClass="form-control"
                              placeholder="Lead artykułu"
                              defaultValue={this.state.lead}
                              value={this.state.lead}
                            />
                          </FormGroup>
                        </Col>
                      </Row>

                      {/* {
                            ...inputs.lead,
                            inputRef: ref => {
                              this.lead = ref;
                            },
                            value: this.state.lead,
                            onChange: this.handleChange,
                            as: "textarea",
                          }, */}

                      <FormGroup controlId="formControlsSelect">
                        <ControlLabel>Kategoria</ControlLabel>
                        <FormControl
                          componentClass="select"
                          placeholder="kategoria"
                          inputRef={ref => {
                            this.category = ref;
                          }}
                          name="category"
                          value={this.state.category}
                          onChange={this.handleChange}
                        >
                          {categories.map(category => (
                            <option value={category._id} key={category._id}>
                              {category.name}
                            </option>
                          ))}
                        </FormControl>
                      </FormGroup>
                      {this.state.articleImage ? (
                        <div style={styles.flexContainer}>
                          <img
                            src={`${api_url}/${this.state.articleImage}`}
                            style={styles.imgThumbnail}
                          />
                          <div style={styles.borderContainer}>
                            <FormInputs
                              ncols={["col-md-5"]}
                              proprieties={[
                                {
                                  ...inputs.changeImage,
                                  inputRef: ref => {
                                    this.changeImage = ref;
                                  },
                                },
                              ]}
                            />
                            <Button
                              style={styles.smallButtonStyle}
                              fill
                              onClick={this.changArticleImage}
                            >
                              Zmień
                            </Button>
                          </div>
                        </div>
                      ) : (
                        <FormInputs
                          ncols={["col-md-5"]}
                          proprieties={[
                            {
                              ...inputs.image,
                              inputRef: ref => {
                                this.articleImage = ref;
                              },
                            },
                          ]}
                        />
                      )}
                      <Editor
                        editorState={editorState}
                        editorStyle={styles.wysiwygStyle}
                        toolbarStyle={styles.inputStyle}
                        onEditorStateChange={this.onEditorStateChange}
                        toolbar={{
                          image: {
                            urlEnabled: true,
                            uploadEnabled: true,
                            uploadCallback: this.uploadFileFromWysiwyg,
                          },
                        }}
                      />
                      <FormInputs
                        ncols={["col-md-12"]}
                        proprieties={[
                          {
                            ...inputs.tags,
                            value: this.state.tags,
                            onChange: this.handleChange,
                          },
                        ]}
                      />
                      <p style={styles.smallText}>
                        (wypisz tagi oddzielone tylko przecinkami, np:
                        malarstwo,rzeźba,kubizm,van gogh,obraz)
                      </p>

                      <div className="clearfix" />
                    </form>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return {
    articles: state.articles,
    categories: state.categories,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    articleActions: bindActionCreators(articleActions, dispatch),
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(NewPost);
