export const styles = {
  formContainer: {
    marginLeft: 40,
    marginRight: 40,
  },
  buttonContainer: {
    marginBottom: 20,
  },
  inputStyle: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#E3E3E3",
    borderRadius: 4,
    color: "#565656",
    padding: 12,
  },
  wysiwygStyle: {
    borderWidth: 1,
    borderStyle: "solid",
    borderColor: "#E3E3E3",
    borderRadius: 4,
    color: "#565656",
    padding: 12,
    height: 450,
  },
  buttonStyle: {
    margin: 5,
  },
  smallButtonStyle: {
    paddingTop: 2,
    paddingBottom: 2,
    height: 26,
  },
  smallText: {
    fontSize: 12,
    marginBottom: 5,
    color: "#9a9a9a",
  },
  imgThumbnail: {
    width: "300px",
    marginBottom: 15,
  },
  flexContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
  },
  borderContainer: {
    display: "flex",
    alignItems: "center",
    justifyContent: "space-between",
    borderColor: "#E3E3E3",
    borderWidth: 1,
    borderStyle: "solid",
    borderRadius: 4,
    padding: 12,
    margin: 15,
  },
};
