export const inputs = {
  title: {
    label: "Tytuł artykułu",
    name: "title",
    type: "text",
    bsClass: "form-control",
    required: true,
  },
  lead: {
    label: "lead",
    name: "lead",
    type: "text",
    bsClass: "form-control",
    required: true,
  },
  changeImage: {
    label: "Zmień zdjęcie",
    name: "changeImage",
    type: "file",
    bsClass: "form-control",
    required: true,
  },
  image: {
    label: "Zdjęcie artykułu",
    name: "articleImage",
    type: "file",
    bsClass: "form-control",
    required: true,
  },
  tags: {
    label: "Tagi",
    name: "tags",
    type: "text",
    bsClass: "form-control",
    required: true,
  },
};
