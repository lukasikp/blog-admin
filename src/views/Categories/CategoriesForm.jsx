import React, { Component } from "react";
import { connect } from "react-redux";
import { createCategory, editCategory } from "../../actions/categoryActions";

import { FormGroup, ControlLabel, FormControl } from "react-bootstrap";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";

class CategoriesForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: "",
      main_category: true,
      parent_category: null,
      active: true,
      color: "",
      showSelect: false,
      editForm: false,
    };
  }

  componentDidMount() {
    this.initState();
  }

  initState = () => {
    const category = this.props.editCategoryData;
    category === null
      ? this.setState({ editForm: false })
      : this.setState({
          editForm: true,
          name: category.name,
          _id: category._id,
          main_category: category.relations.main_category,
          parent_category: category.relations.parent_category,
          active: category.active,
          color: category.color || "",
          showSelect: category.relations.main_category ? false : true,
        });
  };

  submit = e => {
    e.preventDefault();
    const requestData = {
      name: this.state.name,
      main_category: this.state.main_category,
      parent_category: this.state.parent_category,
      color: this.state.color,
      active: this.state.active,
    };
    if (!this.state.editForm) {
      this.props.createCategory(this.props.token, requestData);
    } else {
      this.props.editCategory(this.state._id, this.props.token, requestData);
    }
  };

  toggleCheckbox = e => {
    const { name, checked } = e.target;
    this.setState({ [name]: checked });
    name === "main_category" &&
      this.setState({
        showSelect: !this.state.showSelect,
        parent_category: null,
      });
  };

  handleChange = e => {
    this.setState({ [e.target.name]: e.target.value });
  };

  render() {
    const { categories } = this.props;
    const mainCategories = categories.filter(
      category => category.relations.main_category === true
    );
    return (
      <form onSubmit={this.submit} style={styles.formContainer}>
        <div style={styles.inputContainer}>
          <p
            className="pe-7s-close"
            style={styles.closeIcon}
            onClick={this.props.toggleForm}
          />
          <FormInputs
            ncols={["col-md-12"]}
            proprieties={[
              {
                label: "Nazwa kategorii",
                name: "name",
                type: "text",
                bsClass: "form-control",
                required: true,
                onChange: this.handleChange,
                value: this.state.name,
              },
            ]}
          />
          <FormInputs
            ncols={["col-md-12"]}
            proprieties={[
              {
                label: "Kolor kategorii",
                name: "color",
                type: "text",
                bsClass: "form-control",
                required: true,
                onChange: this.handleChange,
                value: this.state.color,
              },
            ]}
          />
          <FormGroup style={styles.row}>
            <label>
              <input
                name="main_category"
                type="checkbox"
                checked={this.state.main_category}
                onChange={this.toggleCheckbox}
              />
              Główna kategoria
            </label>
          </FormGroup>
          {this.state.showSelect && (
            <FormGroup controlId="formControlsSelect">
              <ControlLabel>Podaj kategorię nadrzędną</ControlLabel>
              <FormControl
                componentClass="select"
                placeholder="głowne kategorie"
                name="parent_category"
                value={this.state.parent_category || -1}
                onChange={this.handleChange}
              >
                <option disabled value={-1} key={-1}>
                  Wybierz
                </option>
                {mainCategories.map(category => (
                  <option value={category._id} key={category._id}>
                    {category.name}
                  </option>
                ))}
              </FormControl>
            </FormGroup>
          )}
          <FormGroup style={styles.row}>
            <label>
              <input
                name="active"
                type="checkbox"
                checked={this.state.active}
                onChange={this.toggleCheckbox}
              />
              Aktywna
            </label>
          </FormGroup>
        </div>
        <Button bsStyle="info" fill type="submit">
          {this.state.editForm ? "Edytuj kategorię" : "Dodaj kategorię"}
        </Button>
      </form>
    );
  }
}

const styles = {
  formContainer: {
    position: "relative",
    padding: 20,
    width: "100%",
    backgroundColor: "#fbfbfb",
    borderColor: "rgb(221, 221, 221)",
    borderRadius: "4px",
    borderWidth: "1px",
    borderStyle: "solid",
  },
  closeIcon: {
    position: "absolute",
    top: 20,
    right: 20,
    fontSize: 40,
    cursor: "pointer",
    color: "#1DC7EA",
  },
  inputContainer: {
    width: 400,
    display: "block",
  },
  row: {
    display: "flex",
    alignItems: "center",
  },
};

function mapStateToProps(state) {
  return {
    categories: state.categories.categoryList.result,
    token: state.user.token,
  };
}

export default connect(
  mapStateToProps,
  { createCategory, editCategory }
)(CategoriesForm);
