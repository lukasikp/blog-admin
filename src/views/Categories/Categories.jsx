import React, { Component } from "react";
import { connect } from "react-redux";
import { bindActionCreators } from "redux";
import * as categoryActions from "../../actions/categoryActions";
import { Grid, Row, Col, Table, MenuItem, Modal } from "react-bootstrap";

import Card from "components/Card/Card.jsx";
import { FormInputs } from "components/FormInputs/FormInputs.jsx";
import Button from "components/CustomButton/CustomButton.jsx";
import { thArray, tdArray } from "variables/Variables.jsx";

import { style } from "variables/Variables.jsx";

import CategoriesForm from "./CategoriesForm";

const postThArray = ["Lp", "Kategoria", "Główna kategoria", "Aktywna"];

class Categories extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCategoriesForm: false,
      editCategory: null,
      showPopup: false,
      showNotification: false,
      removeCategoryByName: "",
      removeCategoryById: "",
    };
  }

  componentDidMount() {
    this.props.categoryActions.fetchCategoryList();
  }

  componentWillReceiveProps(newProps) {
    if (
      JSON.stringify(this.props.categories.categoryResponse) !==
      JSON.stringify(newProps.categories.categoryResponse)
    ) {
      this.props.showNotification(
        newProps.categories.categoryResponse.status,
        newProps.categories.categoryResponse.data.message
      );
    }
  }

  removeCategory = () => {
    this.props.categoryActions.removeCategory(
      this.state.removeCategoryById,
      this.props.user.token
    );
    this.handleClosePopup();
  };

  handleClosePopup = () => {
    this.setState({ showPopup: false });
  };

  handleShowPopup = (_id, categoryName) => {
    this.setState({
      removeCategoryByName: categoryName,
      removeCategoryById: _id,
    });
    this.setState({ showPopup: true });
  };

  toggleForm = () => {
    this.setState({ showCategoriesForm: !this.state.showCategoriesForm });
  };

  createCategory = () => {
    this.setState({ showCategoriesForm: true, editCategory: null });
  };

  editCategory = category => {
    this.setState({ showCategoriesForm: true, editCategory: category });
  };

  getCategoryName = _id => {
    const obj = this.props.categories.categoryList.result.filter(
      obj => obj._id === _id
    );
    return obj[0].name;
  };

  render() {
    return (
      <div className="content">
        <Grid fluid>
          <Row>
            <Col md={12} />
            <Col md={12}>
              <Card
                ctTableFullWidth
                ctTableResponsive
                content={
                  <div>
                    <div style={styles.content}>
                      {this.state.showCategoriesForm && (
                        <CategoriesForm
                          toggleForm={this.toggleForm}
                          editCategoryData={this.state.editCategory}
                        />
                      )}
                      {!this.state.showCategoriesForm && (
                        <Button
                          bsStyle={
                            this.state.showCategoriesForm ? "default" : "info"
                          }
                          fill
                          onClick={this.createCategory}
                        >
                          Utwórz kategorię
                        </Button>
                      )}
                    </div>
                    <Table striped hover>
                      <thead>
                        <tr>
                          {postThArray.map((prop, key) => {
                            return <th key={key}>{prop}</th>;
                          })}
                        </tr>
                      </thead>
                      <tbody>
                        {this.props.categories.categoryList.hasOwnProperty(
                          "result"
                        ) &&
                          this.props.categories.categoryList.result.map(
                            (prop, key) => {
                              return (
                                <tr key={key}>
                                  <td>{key + 1}</td>
                                  <td>{prop.name}</td>
                                  <td style={{ color: "rgb(154, 154, 154)" }}>
                                    {prop.relations.main_category ? (
                                      <span className="pe-7s-check" />
                                    ) : (
                                      this.getCategoryName(
                                        prop.relations.parent_category
                                      )
                                    )}
                                  </td>
                                  <td>
                                    {prop.active && (
                                      <span className="pe-7s-check" />
                                    )}
                                  </td>
                                  <td>
                                    <span
                                      onClick={() => this.editCategory(prop)}
                                      className="pe-7s-note"
                                      style={styles.icon}
                                    />
                                    <span
                                      onClick={() =>
                                        this.handleShowPopup(
                                          prop._id,
                                          prop.name
                                        )
                                      }
                                      className="pe-7s-trash"
                                      style={styles.icon}
                                    />
                                  </td>
                                </tr>
                              );
                            }
                          )}
                      </tbody>
                    </Table>
                  </div>
                }
              />
            </Col>
          </Row>
        </Grid>

        <Modal show={this.state.showPopup} onHide={this.handleClosePopup}>
          <Modal.Header closeButton>
            <Modal.Title>
              Na pewno usunąć kategorię {this.state.removeCategoryByName}?
            </Modal.Title>
          </Modal.Header>

          <Modal.Footer>
            <Button onClick={this.handleClosePopup} bsStyle="default" fill>
              Anuluj
            </Button>
            <Button bsStyle="danger" onClick={this.removeCategory} fill>
              Usuń kategorię
            </Button>
          </Modal.Footer>
        </Modal>
      </div>
    );
  }
}

const styles = {
  content: {
    padding: 20,
  },
  icon: {
    display: "inline-block",
    paddingLeft: 5,
    paddingRight: 5,
    fontSize: 20,
    cursor: "pointer",
  },
};

function mapStateToProps(state) {
  return {
    categories: state.categories,
    user: state.user,
  };
}

function mapDispatchToProps(dispatch) {
  return {
    categoryActions: bindActionCreators(categoryActions, dispatch),
  };
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Categories);
